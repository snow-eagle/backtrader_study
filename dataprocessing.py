import datetime
import pandas as pd
import os.path
import sys
script_path = os.path.dirname(__file__)
talib_path = os.path.join(script_path,'topq_talib.py')
sys.path.append(talib_path)
import topq_talib as tt

"""本脚本是借助极宽TOPQ_TALBI进行的数据处理，提前处理了各种指标数据，为筛选数据提前准备"""
    # 读取数据
# 读入股票代码
stk_code_num= []
stk_code_file =os.listdir('../data/tdx/day/')
for stk_code in stk_code_file:
    index1 = stk_code.rfind(".")
    newname_code = stk_code[:index1]
    stk_code_num.append(newname_code)

for stk_code in stk_code_num:
    # 读入数据
    input_file ='../data/tdx/day/' + stk_code + '.csv'

    # df = pd.read_csv(input_file, usecols=[1, 2, 3, 4, 5, 6],index_col=0, parse_dates=True)
    df = pd.read_csv(input_file, index_col = 0)
    df = df.sort_index(ascending = True)

    ma_list = [5, 10, 20, 30, 60, 120, 240]
    for i in ma_list:
        df = tt.MA_n(df, i)
    # vol_MA
    # vol_ma_list = [5, 10, 20, 30, 60, 120, 240]
    # for i in vol_ma_list:
    #     df = tt.c(df, i)
    # BBANDS
    df = tt.BBANDS_UpLow(df, 20)
    # MACD
    df = tt.MACD020(df, 12, 26)
    # KDJ
    df = tt.KDJ(df, 9, 3)
    # RSI
    df = tt.RSI(df, 14)
    # 写出文件
    df.to_csv('../data/outdata/%s.csv' % stk_code)

