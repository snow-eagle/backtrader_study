import tushare as ts
import pandas as pd

#***************仅修改代码和日期********************#

stock_code ='000333'
st0='2020-01-01'
st9='2021-11-29'

#****************以下代码不用修改*******************#

stock_name=stock_code+'.csv'
pd =ts.get_k_data(stock_code,start=st0,end=st9)
pd.to_csv(stock_name)
print(pd.head(3),pd.tail(3))