import datetime  # 用于datetime对象操作
import os  # 用于管理路径
import sys  # 用于在argvTo[0]中找到脚本名称
import pandas as pd


# 判断金叉
def golden_crossover(df, fast, slow):
    # K线数目不足，次新股
    if df.shape[0] < slow:
        return False
    # 技术指标名称
    fast_indicator = 'ma_%d' % fast
    slow_indicator = 'ma_%d' % slow

    # 最后一日短期均线在长期均线上方，且倒数第二日短期均线在长期均线下方
    return df[fast_indicator][df.index[0]] > df[slow_indicator][df.index[0]] \
        and df[fast_indicator][df.index[1]] < df[slow_indicator][df.index[1]]


# 读入股票代码
stk_code_num= []
# stk_code_file =os.listdir('../data/outdata/')
# for stk_code in stk_code_file:
#     index1 = stk_code.rfind(".")
#     newname_code = stk_code[:index1]
#     stk_code_num.append(newname_code)
#     # stk_path = '../data/tdx/day/'+stk_code
#     # stk_data = pd.read_csv(stk_path, encoding='gbk')
#     # stk_data['code']=newname_code
# # stk_pools = pd.read_csv(stk_code_file, encoding = 'gbk')
# # out_df = pd.DataFrame(columns=['code'], dtype = str)

# 读入股票代码  ../data/tdx/all_codes.csv/文件是之前处理的代码列表数据
stk_code_file = '../data/tdx/all_codes.csv'
stk_code_num = pd.read_csv(stk_code_file, encoding = 'gbk')
out_df = pd.DataFrame(columns=['code'], dtype = str)

# 对每个股票进行判断
for stk_code in stk_code_num['code']:
    # 读入数据 ../data/outdata/文件夹是存放之前处理的数据
    input_file ='../data/outdata/' + stk_code + '.csv'
    df = pd.read_csv(input_file, index_col = 0)
    df = df.sort_index(ascending=False)
    if golden_crossover(df, 5, 60):
        out_df = out_df.append({'code': stk_code}, ignore_index = True)
        print(stk_code)
out_df.to_csv('../data/output/stock_picking.csv')   # 输出满足要求的股票代码
